const fs = require('fs')
const path = require('path')
const bcrypt = require('bcryptjs')

const pswrdsPath = path.join(__dirname, 'passwords.json')
let passwords = JSON.parse(fs.readFileSync(pswrdsPath, 'utf-8'))

const setPassword = (filename, pswrd) => {
  passwords[filename] = bcrypt.hashSync(pswrd, 8)
  fs.writeFileSync(pswrdsPath, JSON.stringify(passwords, null, 2));
}

const checkFile = (filename, pswrd = '') => passwords.hasOwnProperty(filename) 
  ? bcrypt.compareSync(pswrd, passwords[filename]) 
  : true

const deletePassword = (filename) => {
  delete passwords[filename]
  fs.writeFileSync(pswrdsPath, JSON.stringify(passwords, null, 2));
}
module.exports = {
  setPassword,
  checkFile,
  deletePassword
}

