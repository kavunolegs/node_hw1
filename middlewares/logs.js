const fs = require('fs')
const path = require('path')

module.exports.logs = (req, res, next) => {
  let time = new Date()
  let hour = time.getHours()
  let minutes = time.getMinutes()
  let seconds = time.getSeconds()

  let addZeroes = time => time < 10 ? '0' + time : time

  let info = `${addZeroes(hour)}:${addZeroes(minutes)}:${addZeroes(seconds)}, method: ${req.method}, url: ${req.url}`

  console.log(info)
  fs.appendFile(path.join(__dirname, '..', 'log.log'), info + "\n", () => {})
  next()
}