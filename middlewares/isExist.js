const fs = require('fs')
const path = require('path')

module.exports.isExist = (req, res, next) => {
  let fileName = req.params.fileName
  let filePath = path.join(__dirname, '..', '/files/', fileName)

  if (!fs.existsSync(filePath)) {
    return res.status(400).json({message:`No file with '${fileName}' filename found`})
  }
  
  next()
}