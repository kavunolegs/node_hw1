const { checkFile } = require('../passwords/passwords')

const checkPswrd = async (req, res, next) => {
  let fileName = req.params.fileName
  let isAvailable = await checkFile(fileName, req.params.password??req.query.password)

  if (!isAvailable) {
    return res.status(400).json({message:`Wrong or missing password`})
  }

  next()
}

module.exports = { checkPswrd }