const express = require('express')
const router = express.Router()
const config = require('config')

const fs = require('fs')
const path = require('path')
const {setPassword, checkFile, deletePassword} = require('./passwords/passwords')
const {isExist} = require('./middlewares/isExist')
const {checkPswrd} = require('./middlewares/checkPswrd')

const ENCODING = config.get('file_encoding')

router.get('/', (req, res) => {
  try {
    let files = fs.readdirSync(path.join(__dirname, '/files'))

    res.status(200).json({
      message: "Success",
      files
    })

  } catch (e) {
    res.status(500).json({message:"Server error"})
  }
})

router.post('/', (req, res) => {
  try {
    const {filename, content, password} = req.body
    let isCorrectName = /\.(log|txt|json|yaml|xml|js)$/.test(filename)

    if (!isCorrectName) {
      return res.status(400).json({message:"Please specify correct 'filename' parameter"})
    }

    if(!fs.existsSync(path.join(__dirname, '/files'))) {
      fs.mkdirSync(path.join(__dirname, '/files'))
    }

    let nameExist = fs.existsSync(path.join(__dirname, '/files/', filename))
    
    if (nameExist) {
      return res.status(400).json({message:`${filename} already exist`})
    }

    if (!content) {
      return res.status(400).json({message:"Please specify 'content' parameter"})
    }
    
    fs.writeFileSync(path.join(__dirname, '/files/', filename), content, ENCODING)

    password && setPassword(filename, password)

    res.status(200).json({message:"File created successfully"})
  } catch (e) {
    res.status(500).json({message:"Server error"})
  }
})

router.get('/:fileName/:password?', isExist, checkPswrd, async (req, res) => {
  try {
    let fileName = req.params.fileName
    let filePath = path.join(__dirname, '/files/', fileName)

    let stats = fs.statSync(filePath)
    let content = fs.readFileSync(filePath, ENCODING)

    res.status(200).json({
      message: "Success",
      filename: fileName,
      content,
      extension: /(?<=\.).{2,}$/.exec(fileName)[0],
      uploadedDate: stats.birthtime
    })
  } catch (e) {
    res.status(500).json({message:"Server error"})
  }
})

router.put('/:fileName/:password?', isExist, checkPswrd, async (req, res) => {
  try {
    let fileName = req.params.fileName
    let filePath = path.join(__dirname, '/files/', fileName)
    let {content} = req.body

    if (!content) {
      return res.status(400).json({message:`No content to updating '${fileName}'`})
    }

    fs.writeFileSync(filePath, content, ENCODING)

    res.status(200).json({
      message: "Updated"
    })
  } catch (e) {
    res.status(500).json({message:"Server error"})
  }
})

router.delete('/:fileName/:password?', isExist, checkPswrd, async (req, res) => {
  try {
    let fileName = req.params.fileName
    let filePath = path.join(__dirname, '/files/', fileName)

    await deletePassword(fileName)
    fs.unlinkSync(filePath)

    res.status(200).json({
      message: "Deleted"
    })
  } catch (e) {
    res.status(500).json({message:"Server error"})
  }
})

module.exports = router