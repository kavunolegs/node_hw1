const express = require('express')
const app = express()
const config = require('config')
const {logs} = require('./middlewares/logs')

const PORT = config.get('PORT')

app.use(express.json())
app.use(logs)
app.use('/api/files', require('./files.routes'))
app.use((err, req, res, next) => {
  res.status(500).send({error:"Something go wrong!"})
})

app.listen(PORT, () => {
  console.log(`Server started on ${PORT}!!!`)
})